﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using tdd_labb2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tdd_labb2.Tests
{
    [TestClass()]
    public class ProgramTests
    {
        [TestMethod()]
        public void StringCounterTest()
        {
            //arrange
            var stringToTest = "Hej du!";
            var expected = 7;

            //act
            var result = Program.StringCounter(stringToTest);   

            //assert
            Assert.AreEqual(result,expected,"Det matchar inte...");
        }
    }
}